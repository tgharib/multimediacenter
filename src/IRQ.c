/******************************************************************************/
/* IRQ.C: IRQ Handlers                                                        */
/******************************************************************************/
/* This file is part of the uVision/ARM development tools.                    */
/* Copyright (c) 2005-2009 Keil Software. All rights reserved.                */
/* This software may only be used under the terms of a valid, current,        */
/* end user licence from KEIL for a compatible version of KEIL software       */
/* development tools. Nothing else gives you the right to use this software.  */
/******************************************************************************/

unsigned long ticks = 0;

void SysTick_Handler(void) { /* SysTick Interrupt Handler (10ms)   */
    ticks++;
    if (ticks == 100) {
        ticks = 0;
    }
}
