#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "GLCD.h"
#include "KBD.h"

#define __FI 1         // Font index 16x24
extern uint8_t ticks;  // Import external variables from IRQ.c file

// display is 10 lines (height) of 20 characters (width)
char display2DArray[10][21];  // first index is height and second index is width
int snakeX[20], snakeY[20];
int snakeMotionX, snakeMotionY;
int snakeLength;
int foodX, foodY;

void clearDisplayArray() {
    int i;
    for (i = 0; i < 10; i++) {
        strcpy(display2DArray[i], "                    ");
    }
}

void takeInput() {
    uint32_t joystickValue = get_button();

    switch (joystickValue) {
        case KBD_UP: {
            if (snakeMotionY != 1) {
                snakeMotionY = -1;
                snakeMotionX = 0;
            }
        } break;

        case KBD_RIGHT: {
            if (snakeMotionX != -1) {
                snakeMotionY = 0;
                snakeMotionX = 1;
            }
        } break;

        case KBD_DOWN: {
            if (snakeMotionY != -1) {
                snakeMotionY = 1;
                snakeMotionX = 0;
            }
        } break;

        case KBD_LEFT: {
            if (snakeMotionX != 1) {
                snakeMotionY = 0;
                snakeMotionX = -1;
            }
        } break;
    }
}

void createNewFood() {
    foodX = rand() % 20;
    foodY = rand() % 10;
}

bool snakePhysics() {
    int i;

    // remove the snake tail from display array
    display2DArray[snakeY[snakeLength - 1]][snakeX[snakeLength - 1]] = ' ';

    // make space for a new snake head
    for (i = snakeLength - 1; i > 0; i--) {
        snakeY[i] = snakeY[i - 1];
        snakeX[i] = snakeX[i - 1];
    }

    // motion is applied to snake head
    snakeX[0] += snakeMotionX;
    snakeY[0] += snakeMotionY;

    // if snake head hits a display boundary, game is over
    if (snakeX[0] < 0 || snakeX[0] >= 20 || snakeY[0] < 0 || snakeY[0] >= 10) {
        GLCD_Clear(White);
        GLCD_DisplayString(5, 0, __FI, "      You lose!     ");
        return false;
    }

    // if snake head hits itself, game is over
    for (i = snakeLength - 1; i > 0; i--) {
        if (snakeX[i] == snakeX[0] && snakeY[i] == snakeY[0]) {
            GLCD_Clear(White);
            GLCD_DisplayString(5, 0, __FI, "      You lose!     ");
            return false;
        }
    }

    // if snake head hits a food, increase snake length
    if (snakeX[0] == foodX && snakeY[0] == foodY) {
        createNewFood();
        snakeLength++;

        if (snakeLength == 20) {  // if snake reaches length 20, you win
            GLCD_Clear(White);
            GLCD_DisplayString(5, 0, __FI, "      You win!     ");
            return false;
        }
    }

    // draw the new snake head to display array
    display2DArray[snakeY[0]][snakeX[0]] = 'o';
    return true;
}

void drawDisplayArrayToDisplay() {
    int i;
    for (i = 0; i < 10; i++) {
        GLCD_DisplayString(i, 0, __FI, (unsigned char *)display2DArray[i]);
    }
}

void Snake_Game() {
    snakeX[0] = 10;
    snakeY[0] = 2;
    snakeMotionX = 1;
    snakeMotionY = 0;
    snakeLength = 1;
    foodX = 0;
    foodY = 0;

    clearDisplayArray();
    createNewFood();

    while (true) {
        takeInput();

        if (ticks == 0) {                        // true once every second
            display2DArray[foodY][foodX] = 'x';  // draw food
            if (!snakePhysics()) {
                break;
            }
            drawDisplayArrayToDisplay();
        }
    }
}
